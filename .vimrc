set tabstop=4
set shiftwidth=4
set background=dark
set nowrap

set nocompatible               " be iMproved
 filetype off                   " required!

 set rtp+=~/.vim/bundle/vundle/

 call vundle#rc()
 
 let g:vundles=['general', 'programming', 'php', 'javascript', 'html', 'misc']

 " let Vundle manage Vundle
 " required! 
 Bundle 'gmarik/vundle'
 Bundle 'evidens/vim-twig'
 
 " General
 if count(g:vundles, 'general')
 Bundle 'scrooloose/nerdtree'
 Bundle 'altercation/vim-colors-solarized'
 Bundle 'tpope/vim-surround'
 Bundle 'YankRing.vim'
 let g:yankring_history_dir = $HOME.'/.vim/'
 let g:yankring_history_file = '.yankring_history'
 Bundle 'kien/ctrlp.vim'
 Bundle 'jistr/vim-nerdtree-tabs'
 Bundle 'flazz/vim-colorschemes'
 Bundle 'corntrace/bufexplorer'
 Bundle 'austintaylor/vim-indentobject'
 Bundle 'SearchComplete'
 Bundle 'sudo.vim'
 Bundle 'bronson/vim-trailing-whitespace'
 Bundle 'mbbill/undotree'
 Bundle 'mhinz/vim-signify'
 if executable('git')
 Bundle 'tpope/vim-fugitive'
 endif
 if executable('hg')
 Bundle 'k-takata/hg-vim'
 endif
 Bundle 'bling/vim-airline'
 if v:version > 703
 Bundle "myusuf3/numbers.vim"
 endif
 endif		 

 " General Programming
 if count(g:vundles, 'programming')
 Bundle 'godlygeek/tabular'
 if executable('ack')
 Bundle 'mileszs/ack.vim'
 endif
 if v:version > 700
 Bundle 'scrooloose/syntastic'
 if executable('ctags')
 Bundle 'majutsushi/tagbar'
 endif
 Bundle 'Shougo/neocomplcache'
 Bundle 'Shougo/neosnippet'
 Bundle 'scrooloose/snipmate-snippets'
 Bundle 'honza/vim-snippets'
 Bundle 'Indent-Guides'
 endif
 endif
                                                    
 " PHP
 if count(g:vundles, 'php')
 Bundle 'spf13/PIV'
 Bundle 'docteurklein/vim-symfony'
 endif

 " Javascript
 if count(g:vundles, 'javascript')
 Bundle 'leshill/vim-json'
 Bundle 'groenewege/vim-less'
 Bundle 'taxilian/vim-web-indent'
 endif
 
 " HTML
 if count(g:vundles, 'html')
 Bundle 'HTML-AutoCloseTag'
 Bundle 'ChrisYip/Better-CSS-Syntax-for-Vim'
 endif 
 
 colorscheme 256-jungle

 autocmd vimenter * NERDTree

 filetype plugin indent on     " required!
 "
 " Brief help
 " :BundleList          - list configured bundles
 " :BundleInstall(!)    - install(update) bundles
 " :BundleSearch(!) foo - search(or refresh cache first) for foo
 " :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
 "
 " see :h vundle for more details or wiki for FAQ
 " NOTE: comments after Bundle command are not allowed..
